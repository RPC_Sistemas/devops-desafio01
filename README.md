# change UrlJenkins
sudo vi /var/lib/jenkins/jenkins.model.JenkinsLocationConfiguration.xml

# Docker
https://docs.docker.com/engine/install/

comandos inline:

docker build -t desafio01 .
docker tag desafio01 rpcsistemas/desafio01
docker push rpcsistemas/desafio01

docker container run -itd -p 8080:8080 rpcsistemas/desafio01:6


# Jenkins
https://www.jenkins.io/doc/book/installing/
## Ubuntu

curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee \
  /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install jenkins

## Java

$ sudo apt update
$ sudo apt install openjdk-11-jre
$ java -version
openjdk version "11.0.12" 2021-07-20
OpenJDK Runtime Environment (build 11.0.12+7-post-Debian-2)
OpenJDK 64-Bit Server VM (build 11.0.12+7-post-Debian-2, mixed mode, sharing)

sudo systemctl enable jenkins
You can start the Jenkins service with the command:

sudo systemctl start jenkins
You can check the status of the Jenkins service using the command:

sudo systemctl status jenkins

# GitLab Plugin para Jenkins
https://plugins.jenkins.io/gitlab-plugin/

# GitLab Webhook para Jenkins
https://docs.gitlab.com/ee/integration/jenkins.html

# Linux

#!/bin/bash
sudo yum update -y
sudo amazon-linux-extras install epel -y
sudo yum install daemonize -y
sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
sudo yum upgrade
sudo amazon-linux-extras install java-openjdk11 -y
sudo yum install jenkins -y
sudo systemctl daemon-reload
sudo systemctl start jenkins
sudo cat /var/lib/jenkins/secrets/initialAdminPassword